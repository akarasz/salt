#!/bin/bash

screen -ls | grep dst_overworld >/dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "Stop Master shard (dst_overworld)."
  screen -S dst_overworld -X stuff $'\003'
  screen -S dst_overworld -X quit
fi

screen -ls | grep dst_caves >/dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "Stop Caves shard (dst_caves)."
  screen -S dst_caves -X stuff $'\003'
  screen -S dst_caves -X quit
fi

